# Labo 11: Tests

L'objectif de ce laboratoire est de se familiariser avec la mise en place de
cadres de tests. Pour cela, nous allons appliquer le *développement dirigé par
les tests* (en anglais, *test driven development* ou TDD):

1. Écrire un test qui ne fonctionne pas
2. Écrire un code minimum pour le faire fonctionner
3. Améliorer le code (éliminer toute duplication)

## 1 - Récupération du dépôt (10 minutes)

* Rendez-vous sur le dépôt
  [set](https://gitlab.com/sergedelil/set)
* Faites en une copie (*fork*)
* Clonez le projet en local sur votre machine
* Puis étudiez les fichiers qui le composent
* Lancez la suite de tests

## 2 - Factorisation (30 minutes)

* On remarque une certaine redondance dans les fonctions `set_contains`,
  `set_add` et `set_remove` qui doivent toutes rechercher si un élément est
  présent ou non dans l'ensemble.
* Modifiez le code afin de minimiser cette redondance. Exploitez au maximum la
  fonction `bsearch` pour que les 3 fonctions l'utilisent pour localiser
  l'emplacement de l'élément (actuellement, `set_add` et `set_remove` reposent
  sur une fouille linéaire).
* Confirmez que tout fonctionne toujours en lançant la suite de tests.

## 3 - Ajout de fonctionnalités (60 minutes)

En utilisant l'approche de développement dirigé par les tests, ajoutez les
fonctionnalités suivantes, dans l'ordre de votre choix:

1. Une fonction `set_cardinality` qui retourne la cardinalité d'un ensemble
2. Une fonction `set_minimum` qui retourne l'élément minimum d'un ensemble
3. Une fonction `set_maximum` qui retourne l'élément maximum d'un ensemble
4. Une fonction `set_inter` qui calcule l'intersection de deux ensembles
5. Une fonction `set_union` qui calcule la réunion de deux ensembles
6. Une fonction `set_is_subset` qui vérifie si un ensemble est un sous-ensemble
   d'un autre
