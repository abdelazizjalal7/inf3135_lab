#include <stdio.h>
#include <stdbool.h>

// Couleur d'une carte
enum couleur { COEUR, CARREAU, TREFLE, PIQUE };

// Valeur d'une carte
// 1: as
// 11: valet
// 12: dame
// 13: roi
// 2 à 10: carte régulière
typedef unsigned int valeur;

// Une carte
struct carte {
    enum couleur couleur;
    valeur valeur;
};

// Une main de 5 cartes
typedef struct carte main5[5];

/**
 * Affiche une des quatre couleurs sur stdout
 *
 * @param c  La couleur à afficher
 */
void afficher_couleur(enum couleur c) {
}

/**
 * Affiche une des 13 valeurs sur stdout
 *
 * @param v  La valeur à afficher
 */
void afficher_valeur(valeur v) {
}

/**
 * Affiche une des 52 cartes sur stdout
 *
 * @param carte  La carte à afficher
 */
void afficher_carte(struct carte carte) {
}

/**
 * Affiche une main sur stdout
 *
 * @param carte  La carte à afficher
 */
void afficher_main(main5 m) {
}

/**
 * Affiche les 52 cartes sur stdout
 */
void afficher_cartes(void) {
}

/**
 * Compte le nombre d'occurrences de chaque couleur dans une main
 *
 * @param m             La main
 * @param num_couleurs  Le tableau des occurrences à remplir
 */
void compter_couleurs(main5 m, unsigned int num_couleurs[]) {
}

/**
 * Compte le nombre d'occurrences de chaque valeur dans une main
 *
 * @param m             La main
 * @param num_couleurs  Le tableau des occurrences à remplir
 */
void compter_valeurs(main5 m, unsigned int num_valeurs[]) {
}

/**
 * Retourne vrai si la main forme une couleur
 *
 * Une main est une couleur si toutes les cartes ont la même couleur.
 *
 * @param m  La main
 */
bool est_couleur(main5 m) {
    return true;
}

/**
 * Retourne vrai si la main forme un carré
 *
 * Une main est un carré si elle contient 4 cartes sur 5 de la même valeur.
 *
 * @param m  La main
 */
bool est_carre(main5 m) {
    return true;
}

/**
 * Retourne vrai si la main est pleine
 *
 * Une main est pleine si elle contient 3 cartes de même valeur et 2 autres de
 * même valeur.
 *
 * @param m  La main
 */
bool est_main_pleine(main5 m) {
    return true;
}

/**
 * Retourne vrai si la main forme une suite
 *
 * Une main est une suite si elle contient 5 valeurs consécutives.
 *
 * @param m  La main
 */
bool est_suite(main5 m) {
    return true;
}

int main(void) {
    afficher_cartes();
    main5 mains[] = {{{CARREAU, 1},  {CARREAU, 2},   {CARREAU, 8},
                      {CARREAU, 11}, {CARREAU, 13}},
                     {{CARREAU, 2},  {TREFLE,  2},   {COEUR,   8},
                      {COEUR,   2},  {PIQUE,   2}},
                     {{PIQUE,   6},  {TREFLE,  7},   {COEUR,   10},
                      {PIQUE,   9},  {CARREAU, 8}},
                     {{PIQUE,   13}, {TREFLE,  9},   {COEUR,   13},
                      {PIQUE,   9},  {CARREAU, 9}}
                    };
    for (unsigned int i = 0; i < 4; ++i) {
        printf("mains[%d] = ", i);
        afficher_main(mains[i]);
        printf("\n");
        printf("Est-ce une suite? %s\n",
                est_suite(mains[i]) ? "oui" : "non");
        printf("Est-ce une couleur? %s\n",
                est_couleur(mains[i]) ? "oui" : "non");
        printf("Est-ce une main pleine? %s\n",
                est_main_pleine(mains[i]) ? "oui" : "non");
        printf("Est-ce un carré? %s\n",
                est_carre(mains[i]) ? "oui" : "non");
    }
    return 0;
}
